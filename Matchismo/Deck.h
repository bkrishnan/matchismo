//
//  Deck.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void) addCard:(Card *)card atTop:(BOOL)atTop;
- (Card *) drawCardAtRandom;
- (NSUInteger) remainingDeckCards;

@end
