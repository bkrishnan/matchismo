//
//  GameScore.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/16/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameScore : NSObject

@property (readonly, nonatomic) NSDate *start;
@property (readonly, nonatomic) NSDate *end;
@property (readonly, nonatomic) NSTimeInterval interval;
@property (strong, readonly, nonatomic) NSString *gameType;
@property (nonatomic) int score;

+ (NSArray *) allScores;

- (id)initWithGameType:(NSString *)gameType;

@end
