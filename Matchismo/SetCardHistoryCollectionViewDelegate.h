//
//  SetCardHistoryCollectionViewDelegate.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/26/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SetCardHistoryCollectionViewDelegate : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

- (void) addPlayRecord:(NSArray * )playRecord;

@end
