//
//  main.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
