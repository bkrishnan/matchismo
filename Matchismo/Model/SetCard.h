//
//  SetCard.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/17/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

@property (nonatomic) NSUInteger color;
@property (nonatomic) NSUInteger symbol;
@property (nonatomic) NSUInteger number;
@property (nonatomic) NSUInteger shading;


+ (NSUInteger) maxColors;
+ (NSUInteger) maxSymbols;
+ (NSUInteger) maxNumbers;
+ (NSUInteger) maxShadings;

@end
