//
//  SetCard.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/17/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "SetCard.h"

@implementation SetCard


+ (NSUInteger) maxColors
{
    return 3;
}

+ (NSUInteger) maxSymbols
{
    return 3;
}

+ (NSUInteger) maxNumbers
{
    return 3;
}

+ (NSUInteger) maxShadings
{
    return 3;
}


- (void)setColor:(NSUInteger)color
{
    if (color < [SetCard maxColors]) {
        _color = color;
    }
}

- (void)setSymbol:(NSUInteger)symbol
{
    if (symbol < [SetCard maxSymbols]) {
        _symbol = symbol;
    }
}

- (void)setNumber:(NSUInteger)number
{
    if (number <= [SetCard maxNumbers]) {
        _number = number;
    }
}

- (void)setShading:(NSUInteger)shading
{
    if (shading < [SetCard maxShadings]) {
        _shading = shading;
    }
}

//Set cards should also be removed when unplayable
- (void)setUnplayable:(BOOL)unplayable
{
    [super setUnplayable:unplayable];
    [self setMarkedForRemoval:unplayable];
}

- (int)match:(NSArray *)otherCards
{
    int score = 1;
    
    if (otherCards.count == 2) {
        SetCard *otherCard1 = otherCards[0];
        SetCard *otherCard2 = otherCards[1];
        
        if ((self.color != otherCard1.color || self.color != otherCard2.color)
            && (self.color == otherCard1.color || self.color == otherCard2.color || otherCard1.color == otherCard2.color)) {
            score = 0;
        }
        if (score && (self.symbol != otherCard1.symbol || self.symbol != otherCard2.symbol)
            && (self.symbol == otherCard1.symbol || self.symbol == otherCard2.symbol || otherCard1.symbol == otherCard2.symbol)) {
            score = 0;
        }
        if (score && (self.number != otherCard1.number || self.number != otherCard2.number)
            && (self.number == otherCard1.number || self.number == otherCard2.number || otherCard1.number == otherCard2.number)) {
            score = 0;
        }
        if (score && (self.shading != otherCard1.shading || self.shading != otherCard2.shading)
            && (self.shading == otherCard1.shading || self.shading == otherCard2.shading || otherCard1.shading == otherCard2.shading)) {
            score = 0;
        }
    } else {
        score = 0;
    }
    
    return score;
}

@end