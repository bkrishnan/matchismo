//
//  SetCardDeck.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/17/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

- (id)init
{
    self = [super init];
    if (self) {
        for (int color = 0; color < [SetCard maxColors]; color++) {
            for (int symbol = 0; symbol < [SetCard maxSymbols]; symbol++) {
                for (int number = 1; number <= [SetCard maxNumbers]; number++) {
                    for (int shading = 0; shading < [SetCard maxShadings]; shading++) {
                        SetCard *card = [[SetCard alloc] init];
                        card.color = color;
                        card.symbol = symbol;
                        card.number = number;
                        card.shading = shading;
                        [self addCard:card atTop:NO];
                    }
                }
            }
        }
    }
    return self;
}

@end
