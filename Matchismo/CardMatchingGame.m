//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "CardMatchingGame.h"
#import "FlipRecord.h"

@interface CardMatchingGame ()

@property (readwrite, nonatomic) int score;
@property (strong, nonatomic) NSMutableArray *cards;
@property (nonatomic) int numCardsToMatch;
@property (readwrite, nonatomic) Deck *deck;
@property (strong, nonatomic) FlipRecord *flipRecord;
@property (readwrite, strong, nonatomic) NSMutableArray *flipRecordHistory;

@end

@implementation CardMatchingGame

- (NSMutableArray *)flipRecordHistory
{
    if (!_flipRecordHistory) _flipRecordHistory = [[NSMutableArray alloc] init];
    return _flipRecordHistory;
}

- (FlipRecord *)flipRecord
{
    if (!_flipRecord) {
        _flipRecord = [[FlipRecord alloc] init];
        [self.flipRecordHistory insertObject:_flipRecord atIndex:0];
    }
    return _flipRecord;
}

- (void) deleteFlipRecord
{
    [self.flipRecordHistory removeObject:self.flipRecord];
    self.flipRecord = nil;
}

- (NSMutableArray *)cards
{
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (id) initWithCardCount:(NSUInteger)cardCount
               usingDeck:(Deck*)deck
    usingNumCardsToCount:(int) numCardsToMatch
{
    self = [super init];
    
    if (self) {
        _deck = deck;
        for (int i=0; i < cardCount; i++) {
            Card *card = [deck drawCardAtRandom];
            if (!card) {
                self = nil;
                break;
            } else {
                self.cards[i] = card;
            }
        }
        if (numCardsToMatch == 2 || numCardsToMatch == 3) {
            [self setNumCardsToMatch:numCardsToMatch];
        }
    }
    return self;
}

- (void)setScore:(int)score
{
    if (!self.isGameOver) {
        _score = score;
    }
}

- (Card *) cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

#define FLIP_COST 1
#define MATCH_BONUS 4
#define MATCH_BONUS_SET 500
#define MISMATCH_PENALTY 2
#define MISMATCH_PENALTY_SET 100
#define FLIP_COST_SET 10

- (void) flipCardAtIndex:(NSUInteger)index
{
    Card *card = self.cards[index];
    
    //0 - Just flipping up
    //>1 - Match
    //-1 - Tried matching and failed
    int matchScore = 0;
    
    //Match game
    if (self.numCardsToMatch == 2) {
        if (!card.isUnplayable) {
            if (!card.isFaceUp) {
                //Check for a match when flipping up a card
                for (Card *otherCard in self.cards) {
                    if (otherCard.isFaceUp && !otherCard.isUnplayable) {
                        matchScore = [card match:@[otherCard]];
                        if (matchScore) {
                            card.unplayable = YES;
                            otherCard.unplayable = YES;
                            matchScore = matchScore * MATCH_BONUS;
                            self.score += matchScore;
                        } else {
                            otherCard.faceUp = NO;
                            matchScore -= MISMATCH_PENALTY;
                            self.score += matchScore;
                        }
                        break;
                    }
                }
                self.score -= FLIP_COST;
            }
            card.faceUp = !card.isFaceUp;
            [self manageFlipRecord:card score:matchScore];
            if (matchScore < 0) {
                [self.flipRecord.cardsInPlay addObject:card];
            }
        }
    }
    
    //Set game
    if (self.numCardsToMatch == 3) {
        if (!card.isUnplayable) {
            if (!card.isFaceUp) {
                //Check for a match when flipping up a card
                NSMutableArray *otherCards = [[NSMutableArray alloc] init];
                int numOtherCardsOpen = 0;
                for (Card *otherCard in self.cards) {
                    if (otherCard.isFaceUp && !otherCard.isUnplayable) {
                        numOtherCardsOpen++;
                        [otherCards addObject:otherCard];
                    }
                }
                //Only process when 3 cards are open
                if (numOtherCardsOpen == 2) {
                    matchScore = [card match:otherCards];
                    if (matchScore) {
                        card.unplayable = YES;
                        for (Card *otherCard in otherCards) {
                            otherCard.unplayable = YES;
                        }
                        matchScore = matchScore * MATCH_BONUS_SET;
                        self.score += matchScore;
                    } else {
                        matchScore -= MISMATCH_PENALTY_SET;
                        self.score += matchScore;
                        for (Card *otherCard in otherCards) {
                            otherCard.faceUp = NO;
                        }
                    }
                }
                
            }
            card.faceUp = !card.isFaceUp;
            [self manageFlipRecord:card score:matchScore];
            //If the last opened card doesn't make a set, close it
            if (matchScore < 0) card.faceUp = NO;
        }
    }
    if (matchScore > 0) {
        self.wonLatestHand = YES;
    } else {
        self.wonLatestHand = NO;
    }
}

- (void) manageFlipRecord:(Card *) card score:(int)score
{
    //Manage flip records
    int currentCardCount = self.flipRecord.cardsInPlay.count;
    self.flipRecord.score = score;
    if (card.faceUp) {
        //Opened a card
        [self.flipRecord.cardsInPlay addObject:card];
        //If last card in the set is opened, create a new flip record next time
        if (currentCardCount == (self.numCardsToMatch - 1)) {
            self.flipRecord.winningPlay = card.unplayable;
            self.flipRecord = nil;
        }
    } else {
        //Closed a card
        [self.flipRecord.cardsInPlay removeObject:card];
        //If the only card in the set is closed, delete flip record
        if (currentCardCount == 1) {
            [self deleteFlipRecord];
        }
    }
}

- (void) removeCards:(NSArray *)cards
{
    [self.cards removeObjectsInArray:cards];
}

- (NSUInteger) remainingCardsOnBoard
{
    return self.cards.count;
}

- (NSUInteger) remainingCardsinDeck
{
    return self.deck.remainingDeckCards;
}

- (NSArray *) fillGameWithDeckCards:(NSUInteger)drawCount
{
    NSMutableArray *addedCards = [[NSMutableArray alloc] init];
    NSUInteger remainingCards = [self.deck remainingDeckCards];
    if (drawCount > remainingCards) {
        drawCount = remainingCards;
    }
    for (int i = 0; i < drawCount; i++) {
        Card * card = [self.deck drawCardAtRandom];
        [self.cards addObject:card];
        [addedCards addObject:card];
    }
    return addedCards;
}

- (NSUInteger) indexForCard:(Card *)card
{
    return [self.cards indexOfObject:card];
}

#define HINT_PENALTY_SET 450

- (NSArray *) getHint
{
    NSArray *matchingCards = [self findNextMatch];
    if (matchingCards) {
        self.score = self.score - HINT_PENALTY_SET;
    }
    return matchingCards;
}

- (NSArray *) findNextMatch
{
    NSArray *matchingCards = nil;
    if (self.numCardsToMatch == 2) {
        for (int i = 0; i < self.cards.count; i++) {
            Card * currentCard = self.cards[i];
            for (int j = i + 1; j < self.cards.count; j++) {
                Card *otherCard = self.cards[j];
                if ([currentCard match:@[otherCard]]) {
                    return [NSArray arrayWithObjects:currentCard, otherCard, nil];
                }
            }
        }
    } else if (self.numCardsToMatch == 3) {
        for (int i = 0; i < self.cards.count; i++) {
            Card * currentCard = self.cards[i];
            for (int j = i + 1; j < self.cards.count; j++) {
                Card *otherCard1 = self.cards[j];
                for (int k = j + 1; k < self.cards.count; k++) {
                    Card *otherCard2 = self.cards[k];
                    if ([currentCard match:@[otherCard1, otherCard2]]) {
                        return [NSArray arrayWithObjects:currentCard, otherCard1, otherCard2, nil];
                    }
                }
            }
        }
    }
    return matchingCards;
}

@end
