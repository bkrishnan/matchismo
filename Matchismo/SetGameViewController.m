//
//  SetGameViewController.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/17/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "SetGameViewController.h"
#import "SetCardDeck.h"
#import "SetCard.h"
#import "SetCardCollectionViewCell.h"
#import "SetCardView.h"
#import "SetCardHistoryCollectionViewCell.h"
#import "FlipRecord.h"

@interface SetGameViewController ()

@property (weak, nonatomic) IBOutlet UIButton *hintButton;
@property (weak, nonatomic) IBOutlet UIButton *moreCardsButton;
@end

@implementation SetGameViewController

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

#pragma mark Over-ridden methods from base class

- (NSString *) gameName
{
    return @"Set";
}

- (NSString *) collectionViewCellName
{
    return @"SetCard";
}

- (NSString *) collectionViewHistoryCellName
{
    return @"SetCardHistory";
}

- (NSUInteger) startingCardCount
{
    return 12;
}

- (Deck *) createDeck
{
    return [[SetCardDeck alloc] init];
}

- (NSUInteger) numberOfCardsToWin
{
    return 3;
}

- (NSUInteger)numberOfCardsToFetch
{
    return 3;
}

- (void) updateCell:(UICollectionViewCell *)cell forCard:(Card *)card
{
    if ([cell isKindOfClass:[SetCardCollectionViewCell class]]) {
        if ([card isKindOfClass:[SetCard class]]) {
            SetCardCollectionViewCell *setCardCell = (SetCardCollectionViewCell *) cell;
            SetCardView *cardView = setCardCell.setCardView;
            SetCard *setCard = (SetCard *) card;
            [self updateCardView:cardView withCard:setCard];
        }
    }
}

- (void) updateHistoryCell:(UICollectionViewCell *)cell forFlipRecord:(FlipRecord *)flipRecord
{
    if ([cell isKindOfClass:[SetCardHistoryCollectionViewCell class]]) {
        SetCardHistoryCollectionViewCell *historyCell = (SetCardHistoryCollectionViewCell *) cell;
        historyCell.setCardView1.hidden = YES;
        historyCell.setCardView2.hidden = YES;
        historyCell.setCardView3.hidden = YES;
        historyCell.statusImage.backgroundColor = nil;
        NSArray *setCards  = flipRecord.cardsInPlay;
        for (int i = 0; i < setCards.count; i++) {
            SetCard *card = setCards[i];
            SetCardView *cardView = nil;
            switch (i) {
                case 0:
                    cardView = historyCell.setCardView1;
                    break;
                case 1:
                    cardView = historyCell.setCardView2;
                    break;
                case 2:
                    cardView = historyCell.setCardView3;
                    break;
                    
                default:
                    break;
            }
            [self updateCardView:cardView withCard:card];
            cardView.hidden = NO;
            cardView.faceUp = NO;
            cardView.hint = NO;
        }
        if (setCards.count == 3) {
            if (flipRecord.winningPlay) {
                historyCell.statusImage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Right"]];
            } else {
                historyCell.statusImage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Wrong"]];
            }
        }
    }

}

- (void) updateCardView:(SetCardView *)cardView withCard:(SetCard *)card
{
    cardView.color = card.color;
    cardView.symbol = card.symbol;
    cardView.number = card.number;
    cardView.shading = card.shading;
    cardView.faceUp = card.isFaceUp;
    cardView.hint = card.hint;
}

- (void) enableGameUI
{
    [super enableGameUI];
    self.hintButton.enabled = YES;
    self.hintButton.alpha = 1.0;
    self.moreCardsButton.enabled = YES;
    self.moreCardsButton.alpha = 1.0;
}

- (void) disableGameUI
{
    [super disableGameUI];
    self.hintButton.enabled = NO;
    self.hintButton.alpha = 0.3;
    self.moreCardsButton.enabled = NO;
    self.moreCardsButton.alpha = 0.3;
}


@end
