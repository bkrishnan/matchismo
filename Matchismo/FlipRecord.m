//
//  FlipRecord.m
//  Matchismo
//
//  Created by Bharath Krishnan on 3/2/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "FlipRecord.h"

@interface FlipRecord ()

@property (readwrite, strong, nonatomic) NSMutableArray *cardsInPlay;

@end

@implementation FlipRecord

- (id)init
{
    self = [super init];
    if (self) {
        _cardsInPlay = [[NSMutableArray alloc] init];
        _winningPlay = NO;
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"FlipRecord: CardsInPlay:%u, Win:%u", self.cardsInPlay.count, self.winningPlay];
}

@end
