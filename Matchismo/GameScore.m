//
//  GameScore.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/16/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "GameScore.h"

@interface GameScore ()

@property (readwrite, nonatomic) NSDate *start;
@property (readwrite, nonatomic) NSDate *end;
@property (strong, readwrite, nonatomic) NSString *gameType;

@end

@implementation GameScore

#define ALL_KEY @"GameScore_All"
#define SCORE_KEY @"Score_Key"
#define START_KEY @"Start_Key"
#define END_KEY @"End_Key"
#define GAME_TYPE_KEY @"GameType_Key"

+ (NSArray *) allScores
{
    NSMutableArray * scores = [[NSMutableArray alloc] init];
    NSMutableDictionary *gameScores = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_KEY] mutableCopy];
    for (NSDictionary *score in [gameScores allValues]) {
        [scores addObject:[[GameScore alloc] initFromPropsList:score]];
    }
    return scores;
}

- (id)initWithGameType:(NSString *)gameType
{
    self = [super init];
    if (self) {
        _start = [NSDate date];
        _end = _start;
        _score = 0;
        _gameType = gameType;
        if(!_gameType) self = nil;
    }
    return self;
}

//Convenience initializer
- (id) initFromPropsList:(NSDictionary *)score
{
    self = [self init];
    _score = [[score valueForKey:SCORE_KEY] intValue];
    _start = [score valueForKey:START_KEY];
    _end = [score valueForKey:END_KEY];
    _gameType = [score valueForKey:GAME_TYPE_KEY];
    return self;
}

- (void) synchronize
{
    NSMutableDictionary *gameScores = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_KEY] mutableCopy];
    if (!gameScores) gameScores = [[NSMutableDictionary alloc] init];
    [gameScores setObject:[self propsList] forKey:self.start.description];
    [[NSUserDefaults standardUserDefaults] setObject:gameScores forKey:ALL_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (id) propsList
{
    return @{START_KEY: self.start, END_KEY : self.end, SCORE_KEY: @(self.score), GAME_TYPE_KEY: self.gameType};
    
}

- (NSTimeInterval) interval
{
    return [self.end timeIntervalSinceDate:self.start];
}

- (void) setScore:(int)score
{
    _score = score;
    self.end = [NSDate date];
    [self synchronize];
}

@end
