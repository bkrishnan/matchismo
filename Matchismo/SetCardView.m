//
//  SetCardView.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/23/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "SetCardView.h"

@implementation SetCardView

#pragma mark - Drawing

#define CORNER_RADIUS 12.0

#define SHAPE_INSET_PERCENTAGE 0.20
#define SHAPE_VGAP_PERCENT 0.2

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:CORNER_RADIUS];
    
    [roundedRect addClip];
    
    if (self.faceUp) {
        //Yellowish
        [[[UIColor alloc] initWithRed:252.0/255. green:250.0/255. blue:189.0/255. alpha:1.0] setFill];
    } else if (self.isHint) {
        [[UIColor lightGrayColor] setFill];
    } else {
        [[UIColor whiteColor] setFill];
    }
    
    UIRectFill(self.bounds);
    
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];

    CGRect shapesRect = CGRectInset(self.bounds,
                                   self.bounds.size.width * SHAPE_INSET_PERCENTAGE,
                                   self.bounds.size.height * SHAPE_INSET_PERCENTAGE);
    CGFloat shapeWidth = shapesRect.size.width;
    CGFloat shapesHeight = (shapesRect.size.height / 3.0);
    
    
    CGPoint startingPoint = shapesRect.origin;
    CGFloat nextVerticalStart = startingPoint.y;

    switch (self.number) {
        case 1:
            nextVerticalStart = startingPoint.y + shapesHeight;
            break;
        case 2:
            nextVerticalStart = startingPoint.y + (shapesHeight / 2.0);
            break;
        case 3:
            nextVerticalStart = startingPoint.y;
            break;
            
        default:
            break;
    }
    
    [[self cardColor] setFill];
    [[self cardColor] setStroke];

    for (int i = 1; i <= self.number; i++) {
        CGRect rect = CGRectMake(startingPoint.x, nextVerticalStart, shapeWidth, shapesHeight - (shapesHeight * SHAPE_VGAP_PERCENT));
        [self addShapeToRect:rect];
        nextVerticalStart += shapesHeight;
    }
}

- (void) addShapeToRect:(CGRect)rect
{
    UIBezierPath *path = nil;
    switch (self.symbol) {
        case 0:
            path = [self addDiamondToRect:rect];
            break;
        case 1:
            path = [self addSquigglesToRect:rect];
            break;
        case 2:
            path = [self addOvalToRect:rect];
            break;
        default:
            break;
    }
    [self pushContext];
    [self addShadingToPath:path inRect:rect];
    [path addClip];
    [path stroke];
    [self popContext];
}

#define LINE_WIDTH 3.0

- (UIBezierPath *) addDiamondToRect:(CGRect)rect
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(rect.origin.x + (rect.size.width / 2), rect.origin.y)];//top center
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height / 2)];//right center
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height)];//bottom center
    [path addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height / 2)];//left center
    [path closePath];
    path.lineWidth = LINE_WIDTH;
    return path;
}

#define SQUIGGLE_LONG_DIAGONAL_DELTA_X_RATIO 0.594
#define SQUIGGLE_DIAGONAL_DELTA_Y_RATIO 0.12
#define SQUIGGLE_SHORT_DIAGONAL_DELTA_X_RATIO 0.462
#define SQUIGGLE_CONTROL_DELTA_X_RATIO 0.132
#define SQUIGGLE_CONTROL_DELTA_Y_RATIO 0.16

- (UIBezierPath *) addSquigglesToRect:(CGRect)rect
{
    CGPoint middle = CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height / 2);
    CGSize shortDiagonal = CGSizeMake(middle.x * SQUIGGLE_SHORT_DIAGONAL_DELTA_X_RATIO, 6);
    CGSize longDiagonal = CGSizeMake(middle.x * SQUIGGLE_LONG_DIAGONAL_DELTA_X_RATIO, 6);
    CGPoint point1 = CGPointMake(middle.x - longDiagonal.width, middle.y - longDiagonal.height);
    CGPoint point2 = CGPointMake(middle.x + shortDiagonal.width, middle.y - shortDiagonal.height);
    CGPoint point3 = CGPointMake(middle.x + longDiagonal.width, middle.y + longDiagonal.height);
    CGPoint point4 = CGPointMake(middle.x - shortDiagonal.width, middle.y + shortDiagonal.height);
    CGSize controlDelta = CGSizeMake(middle.x * SQUIGGLE_CONTROL_DELTA_X_RATIO, 6);//middle.y * SQUIGGLE_CONTROL_DELTA_Y_RATIO
    CGPoint curve1cp1 = CGPointMake(point1.x+controlDelta.width, point1.y-controlDelta.height);
    CGPoint curve1cp2 = CGPointMake(point2.x-controlDelta.width, point2.y+controlDelta.height);
    CGPoint curve2cp1 = CGPointMake(point2.x+controlDelta.width, point2.y-controlDelta.height);
    CGPoint curve2cp2 = CGPointMake(point3.x+controlDelta.width, point3.y-controlDelta.height);
    CGPoint curve3cp1 = CGPointMake(point3.x-controlDelta.width, point3.y+controlDelta.height);
    CGPoint curve3cp2 = CGPointMake(point4.x+controlDelta.width, point4.y-controlDelta.height);
    CGPoint curve4cp1 = CGPointMake(point4.x-controlDelta.width, point4.y+controlDelta.height);
    CGPoint curve4cp2 = CGPointMake(point1.x-controlDelta.width, point1.y+controlDelta.height);
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    path.lineWidth = LINE_WIDTH;
    [path moveToPoint:point1];
    [path addCurveToPoint:point2 controlPoint1:curve1cp1 controlPoint2:curve1cp2];
    [path addCurveToPoint:point3 controlPoint1:curve2cp1 controlPoint2:curve2cp2];
    [path addCurveToPoint:point4 controlPoint1:curve3cp1 controlPoint2:curve3cp2];
    [path addCurveToPoint:point1 controlPoint1:curve4cp1 controlPoint2:curve4cp2];
    [path closePath];
    return path;
    
}

- (UIBezierPath *) addOvalToRect:(CGRect)rect
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:CORNER_RADIUS];
    path.lineWidth = LINE_WIDTH;
    return path;
}

- (void) addShadingToPath:(UIBezierPath *)path inRect:(CGRect)rect
{
    switch (self.shading) {
        case 0:
            [[UIColor whiteColor] setFill];
            break;
        case 1:
            [[UIColor whiteColor] setFill];
            for (int x = rect.origin.x; x < (rect.origin.x + rect.size.width); x += 5) {
                [path moveToPoint:CGPointMake(x, rect.origin.y)];
                [path addLineToPoint:CGPointMake(x, rect.origin.y + rect.size.height)];
            }
            break;
            
        default:
            break;
    }
    [path fill];
}

- (void)pushContext
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
}

- (void)popContext
{
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
}

- (UIColor *) cardColor
{
    switch (self.color) {
        case 0:
            return [UIColor redColor];
            break;
        case 1:
            //Green
            return [[UIColor alloc] initWithRed:103.0/255. green:177.0/255. blue:92.0/255. alpha:1.0];
            break;
        case 2:
            //Purple
            return [[UIColor alloc] initWithRed:94.0/255. green:91.0/255. blue:176.0/255. alpha:1.0];
            break;
        default:
            return [UIColor blackColor];
            break;
    }
}

- (void)setColor:(NSUInteger)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (void)setSymbol:(NSUInteger)symbol
{
    _symbol = symbol;
    [self setNeedsDisplay];
}

- (void)setNumber:(NSUInteger)number
{
    _number = number;
    [self setNeedsDisplay];
}

- (void)setShading:(NSUInteger)shading
{
    _shading = shading;
    [self setNeedsDisplay];
}

#pragma mark - Initialization

- (void)setup
{
    // do initialization here
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setup];
    return self;
}

@end
