//
//  SettingsViewController.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/21/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *hideMatchedSetsSwitch;

@end

@implementation SettingsViewController

#define HIDE_MATCHED_SETS @"SettingsHideMatchedSets"

- (void) updateUI
{
    self.hideMatchedSetsSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:HIDE_MATCHED_SETS];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateUI];
}

- (void) setup
{
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (IBAction)switchHidingMatchedSets:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:HIDE_MATCHED_SETS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
