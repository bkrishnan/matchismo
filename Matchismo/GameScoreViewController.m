//
//  GameScoreViewController.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/16/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "GameScoreViewController.h"
#import "GameScore.h"

@interface GameScoreViewController ()

@property (weak, nonatomic) IBOutlet UITextView *scoreLabel;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSSortDescriptor *dateDescriptor;
@property (strong, nonatomic) NSSortDescriptor *scoreDescriptor;
@property (strong, nonatomic) NSSortDescriptor *durationDescriptor;
@property (strong, nonatomic) NSArray *currentDescriptors;
@end

@implementation GameScoreViewController

- (IBAction)sortScore:(UIButton *)sender {
    
    switch (sender.tag) {
        case 0:
            self.currentDescriptors = [[NSArray alloc] initWithObjects:self.dateDescriptor, nil];
            break;
        case 1:
            self.currentDescriptors = [[NSArray alloc] initWithObjects:self.scoreDescriptor, self.dateDescriptor, nil];
            break;
        case 2:
            self.currentDescriptors = [[NSArray alloc] initWithObjects:self.durationDescriptor, self.dateDescriptor, nil];
            break;
        default:
            break;
    }
    
    [self updateUI];
}

- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [_dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    }
    return _dateFormatter;
}

- (NSSortDescriptor *)dateDescriptor
{
    if (!_dateDescriptor) _dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"end" ascending:NO];
    return _dateDescriptor;
}

- (NSSortDescriptor *)scoreDescriptor
{
    if (!_scoreDescriptor) _scoreDescriptor = [[NSSortDescriptor alloc] initWithKey:@"score" ascending:NO];
    return _scoreDescriptor;
}

- (NSSortDescriptor *)durationDescriptor{
    if (!_durationDescriptor) _durationDescriptor = [[NSSortDescriptor alloc] initWithKey:@"interval" ascending:YES];
    return _durationDescriptor;
}

- (NSArray *)currentDescriptors
{
    if (!_currentDescriptors) {
        _currentDescriptors = [[NSArray alloc] initWithObjects:self.dateDescriptor, nil];
    }
    return _currentDescriptors;
}

- (void) updateUI
{
    NSString *scoreString = @"";
    
    NSArray *allScores = [GameScore allScores];
    
    allScores = [allScores sortedArrayUsingDescriptors:self.currentDescriptors];
    
    int i = 1;
    for (GameScore *score in allScores) {
        scoreString = [scoreString stringByAppendingString:[NSString stringWithFormat:@"%d. [%@] Score: %d (%@, %0g)\n", i++, score.gameType, score.score, [self.dateFormatter stringFromDate:score.end], round(score.interval)]];
    }
    self.scoreLabel.text = scoreString;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateUI];
}

- (void) setup
{
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

@end
