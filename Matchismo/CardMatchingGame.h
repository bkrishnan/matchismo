//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

//Designated initializer
- (id) initWithCardCount:(NSUInteger)cardCount
               usingDeck:(Deck*)deck
    usingNumCardsToCount:(int) numCardsToMatch;

- (void) flipCardAtIndex:(NSUInteger)index;

- (Card *) cardAtIndex:(NSUInteger)index;

- (void) removeCards:(NSArray *)cards;
- (NSUInteger) remainingCardsOnBoard;
- (NSUInteger) remainingCardsinDeck;
- (NSArray *) fillGameWithDeckCards:(NSUInteger)drawCount;
- (NSUInteger) indexForCard:(Card *)card;
- (NSArray *) findNextMatch;
- (NSArray *) getHint;

@property (readonly, nonatomic) int score;
@property (readonly, strong, nonatomic) NSMutableArray *flipRecordHistory;
@property (nonatomic, getter = isGameOver) BOOL gameOver;
@property (nonatomic) BOOL wonLatestHand;

@end
