//
//  PlayingCardCollectionViewCell.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/23/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCardView.h"

@interface PlayingCardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;

@end
