//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
