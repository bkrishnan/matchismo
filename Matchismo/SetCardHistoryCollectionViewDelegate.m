//
//  SetCardHistoryCollectionViewDelegate.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/26/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "SetCardHistoryCollectionViewDelegate.h"
#import "SetCardHistoryCollectionViewCell.h"
#import "SetCardView.h"
#import "SetCard.h"

@interface SetCardHistoryCollectionViewDelegate ()

@property (strong, nonatomic) NSMutableArray *playRecordHistory;

@end

@implementation SetCardHistoryCollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.playRecordHistory.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void) addPlayRecord:(NSArray * )playRecord
{
    [self.playRecordHistory insertObject:playRecord atIndex:0];
    
}


@end
