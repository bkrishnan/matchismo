//
//  CardGameViewController.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardMatchingGame.h"
#import "FlipRecord.h"

@interface CardGameViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (readonly, nonatomic) NSString *gameName;
@property (readonly, nonatomic) NSUInteger startingCardCount;
@property (readonly, nonatomic) NSUInteger numberOfCardsToWin;
@property (readonly, nonatomic) NSString *collectionViewCellName;
@property (readonly, nonatomic) NSString *collectionViewHistoryCellName;
@property (readonly, nonatomic) NSUInteger numberOfCardsToFetch;
@property (readonly, strong, nonatomic) CardMatchingGame *game;

- (Deck *) createDeck;
- (id) flipRecordDesciptionText:(FlipRecord *)flipRecord;
- (void) updateCell:(UICollectionViewCell *)cell forCard:(Card *)card;
- (void) updateUI;
- (void) enableGameUI;
- (void) disableGameUI;

@end
