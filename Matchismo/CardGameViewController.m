//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/10/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "CardGameViewController.h"
#import "Deck.h"
#import "Card.h"
#import "GameScore.h"
#import "HeaderCollectionReusableView.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (weak, nonatomic) IBOutlet UILabel *scoreLable;
@property (weak, nonatomic) IBOutlet UISlider *playDescriptionSlider;
@property (weak, nonatomic) IBOutlet UILabel *playDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *deckLabel;
@property (readwrite, strong, nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) GameScore *gameScore;
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@property (strong, nonatomic) NSMutableArray *gameHistory;
@property (nonatomic, getter = isShowingHint) BOOL showingHint;
@property (strong, nonatomic) NSArray *hintCards;


@end

@implementation CardGameViewController

#define CARD_CELL_WIDTH 60.0
#define CARD_CELL_HEIGHT 80.0

#define CARD_HISTORY_CELL_WIDTH 280.0
#define CARD_HISTORY_CELL_HEIGHT 80.0

- (NSMutableArray *)gameHistory
{
    if (!_gameHistory) _gameHistory = [[NSMutableArray alloc] init];
    return _gameHistory;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 1:
            return CGSizeMake(CARD_HISTORY_CELL_WIDTH, CARD_HISTORY_CELL_HEIGHT);
        default:
            return CGSizeMake(CARD_CELL_WIDTH, CARD_CELL_HEIGHT);
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return self.gameHistory.count;
            break;
    }
    return [self.game remainingCardsOnBoard];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"Header1" forIndexPath:indexPath];
    if ([header isKindOfClass:[HeaderCollectionReusableView class]]) {
        HeaderCollectionReusableView *customHeader = (HeaderCollectionReusableView *) header;
        switch (indexPath.section) {
            case 1:
                customHeader.headerLabel.text = @"History";
                break;
            default:
                customHeader.headerLabel.text = @"The Game";
                break;
        }
    }
    return header;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    switch (indexPath.section) {
        case 0:
        {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self collectionViewCellName]forIndexPath:indexPath];
            Card *card = [self.game cardAtIndex:indexPath.item];
            [self updateCell:cell forCard:card];
        }
            break;
        case 1:
        {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self collectionViewHistoryCellName]forIndexPath:indexPath];
            FlipRecord *flipRecord = [self.gameHistory objectAtIndex:indexPath.item];
            [self updateHistoryCell:cell forFlipRecord:flipRecord];
        }
            break;
    }
    return cell;
}

- (GameScore *)gameScore
{
    if (!_gameScore) _gameScore = [[GameScore alloc] initWithGameType:[self gameName]];
    return _gameScore;
}

- (CardMatchingGame *)game
{
    if (!_game) {
        _game = [[CardMatchingGame alloc] initWithCardCount:[self startingCardCount] usingDeck:[self createDeck] usingNumCardsToCount:[self numberOfCardsToWin]];
        [self.cardCollectionView reloadData];
    }
    return _game;
}

- (void) resetPlayDescriptionSlider
{
    [self.playDescriptionSlider setMinimumValue:0.0f];
    [self.playDescriptionSlider setMaximumValue:([self.game.flipRecordHistory count] - 1.0)];
    [self.playDescriptionSlider setValue:0.0];
    [self.playDescriptionLabel setBackgroundColor:nil];
    if (self.game.flipRecordHistory.count > 1) {
        [self.playDescriptionSlider setEnabled:YES];
    } else {
        [self.playDescriptionSlider setEnabled:NO];
    }
}

- (void) updateUI
{
    [self cleanupMatchedCards];
    self.scoreLable.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    if (self.game.flipRecordHistory.count) {
        [self setPlayDescriptionLabelText:[self.game.flipRecordHistory objectAtIndex:0]];
    }
    [self resetPlayDescriptionSlider];
    [self updateFlipRecord];
    if (!self.isShowingHint) {
        [self clearHint];
    }
    if (self.game.isGameOver) {
        [self disableGameUI];
        [self.deckLabel setText:@"Game over!"];
    } else {
        [self enableGameUI];
        [self.deckLabel setText:nil];
    }
}

- (void) cleanupMatchedCards
{
    //Remove matched cards
    NSMutableArray *expiredCards = [[NSMutableArray alloc] init];
    NSMutableArray *expiredCardCells = [[NSMutableArray alloc] init];
    for (UICollectionViewCell *cell in self.cardCollectionView.visibleCells) {
        NSIndexPath *index = [self.cardCollectionView indexPathForCell:cell];
        Card * card = [self.game cardAtIndex:index.item];
        if (card.isMarkedForRemoval) {
            [expiredCards addObject:card];
            [expiredCardCells addObject:index];
        } else {
            [self updateCell:cell forCard:card];
        }
    }
    
    if (expiredCards.count) {
        [self.game removeCards:expiredCards];
        //Forced to call reloadData: due to http://openradar.appspot.com/12954582
        [self.cardCollectionView reloadData];
        //[self.cardCollectionView deleteItemsAtIndexPaths:expiredCardCells];
        //Replenish game with fresh cards
        if ([self.game remainingCardsOnBoard] < [self startingCardCount]) {
            [self getMoreCards: nil];
        }
    }

}

- (void) updateFlipRecord
{
    if (self.cardCollectionView.numberOfSections > 0) {
        //Forced to call reloadData: due to http://openradar.appspot.com/12954582
        //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        if (self.gameHistory.count < self.game.flipRecordHistory.count) {
            //Add record
            [self.gameHistory insertObject:[self.game.flipRecordHistory objectAtIndex:0] atIndex:0];
            //[self.cardCollectionView insertItemsAtIndexPaths:@[indexPath]];
        } else if (self.gameHistory.count > self.game.flipRecordHistory.count)
        {
            //Delete record
            [self.gameHistory removeObjectAtIndex:0];
            //[self.cardCollectionView deleteItemsAtIndexPaths:@[indexPath]];
        } else {
            //Refresh record
        }
        [self.cardCollectionView reloadData];
    }

}

- (IBAction) getMoreCards:(UIButton *)sender
{
    if ([self.game remainingCardsinDeck]) {
        NSArray *addedCards = [self.game fillGameWithDeckCards:[self numberOfCardsToFetch]];
        NSMutableArray *addedCardCells = [[NSMutableArray alloc] init];
        NSIndexPath *scrollIndexPath = nil;
        for (int i = 0; i < addedCards.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.game indexForCard:addedCards[i]]inSection:0];
            [addedCardCells addObject:indexPath];
            scrollIndexPath = indexPath;
        }
        if (addedCards.count) {
            //Forced to call reloadData: due to http://openradar.appspot.com/12954582
            [self.cardCollectionView reloadData];
            //[self.cardCollectionView insertItemsAtIndexPaths:addedCardCells];
            if ([self.game remainingCardsOnBoard] > [self startingCardCount]) {
                [self.cardCollectionView scrollToItemAtIndexPath:scrollIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
            }
        }
        if (![self.game remainingCardsinDeck] && ![self.game findNextMatch]) {
            self.game.gameOver = YES;
        }
    } else {
        if (![self.game findNextMatch]) {
            self.game.gameOver = YES;
        } else {
            [self.deckLabel setText:@"No cards left!"];
        }
    }
    
}

- (void) setFlipCount:(int)flipCount
{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (IBAction)flipCard:(UITapGestureRecognizer *)sender {
    if (!self.game.gameOver) {
        CGPoint touchPoint = [sender locationInView:self.cardCollectionView];
        NSIndexPath *index = [self.cardCollectionView indexPathForItemAtPoint:touchPoint];
        if (index) {
            [self.game flipCardAtIndex:index.item];
            self.flipCount++;
            self.gameScore.score = self.game.score;
            if (self.game.wonLatestHand) {
                self.showingHint = NO;
            }
            [self updateUI];
        }
    }
}

- (IBAction)resetGame:(UIButton *)sender {
    if (!self.game.gameOver) {
        UIAlertView *gameNotOverAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to abandon this game?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Yes", nil];
        [gameNotOverAlert show];
    } else {
        [self startNewGame];
    }
}

- (void) startNewGame
{
    [self setGame:nil];
    self.flipCount = 0;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: 0"];
    self.gameScore = nil;
    self.gameHistory = nil;
    self.showingHint = NO;
    [self clearHint];
    [self updateUI];
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    int index = (int) sender.value;
    if (self.game.flipRecordHistory.count > 0) {
        [self setPlayDescriptionLabelText:[self.game.flipRecordHistory objectAtIndex:index]];
    }
    [self.playDescriptionLabel setBackgroundColor: index ? [UIColor lightGrayColor] : nil];
}

- (void)setPlayDescriptionLabelText:(FlipRecord *)flipRecord
{
    id labelText = [self flipRecordDesciptionText:flipRecord];
    if ([labelText isKindOfClass:[NSString class]]) {
        self.playDescriptionLabel.text = labelText;
    }
    if ([labelText isKindOfClass:[NSAttributedString class]]) {
        self.playDescriptionLabel.attributedText = labelText;
    }
}

- (IBAction)showHint {
    if (!self.isShowingHint) {
        self.hintCards = [self.game getHint];
        if (self.hintCards) {
            self.showingHint = YES;
            for (Card *card in self.hintCards) {
                card.hint = YES;
            }
            [self updateUI];
        } else {
            [self.deckLabel setText:@"No set!"];
        }
    }
}

- (void) clearHint
{
    for (Card *card in self.hintCards) {
        card.hint = NO;
    }
}

- (void)viewDidLayoutSubviews
{
    [self updateUI];
}

#pragma mark UIAlertViewDelegate methods

#define REDEAL_CONFIRM_BUTTON_INDEX 1

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView title] isEqualToString:@"Are you sure you want to abandon this game?"] && buttonIndex == REDEAL_CONFIRM_BUTTON_INDEX) {
        [self startNewGame];
    }
}


#pragma mark methods that base classes should "probably" over-ride

- (Deck *) createDeck { return nil; }

- (id) flipRecordDesciptionText:(FlipRecord *)flipRecord { return nil; }

- (void) updateCell:(UICollectionViewCell *)cell forCard:(Card *)card {}

- (void) updateHistoryCell:(UICollectionViewCell *)cell forFlipRecord:(FlipRecord *)flipRecord {}

- (void) enableGameUI {}

- (void) disableGameUI {}

@end
