//
//  SetCardHistoryCollectionViewCell.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/26/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCardView.h"

@interface SetCardHistoryCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet SetCardView *setCardView1;
@property (weak, nonatomic) IBOutlet SetCardView *setCardView2;
@property (weak, nonatomic) IBOutlet SetCardView *setCardView3;
@property (weak, nonatomic) IBOutlet UIView *statusImage;

@end
