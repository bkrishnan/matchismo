//
//  FlipRecord.h
//  Matchismo
//
//  Created by Bharath Krishnan on 3/2/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlipRecord : NSObject

@property (readonly, strong, nonatomic) NSMutableArray *cardsInPlay;
@property (nonatomic, getter = isWinningPlay) BOOL winningPlay;
@property (nonatomic) NSInteger score;
@end
