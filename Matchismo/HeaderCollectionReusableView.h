//
//  HeaderCollectionReusableView.h
//  Matchismo
//
//  Created by Bharath Krishnan on 3/23/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
