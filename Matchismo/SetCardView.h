//
//  SetCardView.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/23/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetCardView : UIView

@property (nonatomic) NSUInteger color;
@property (nonatomic) NSUInteger symbol;
@property (nonatomic) NSUInteger number;
@property (nonatomic) NSUInteger shading;

@property (nonatomic) BOOL faceUp;
@property (nonatomic, getter = isHint) BOOL hint;

@end
