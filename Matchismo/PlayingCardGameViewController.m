//
//  PlayingCardGameViewController.m
//  Matchismo
//
//  Created by Bharath Krishnan on 2/23/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "PlayingCardGameViewController.h"
#import "PlayingCard.h"
#import "PlayingCardDeck.h"
#import "PlayingCardCollectionViewCell.h"
#import "FlipRecord.h"

@interface PlayingCardGameViewController ()

@end

@implementation PlayingCardGameViewController

- (NSString *) gameName
{
    return @"Match";
}

- (NSString *) collectionViewCellName
{
    return @"PlayingCard";
}

- (NSUInteger) startingCardCount
{
    return 22;
}

- (Deck *) createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (NSUInteger) numberOfCardsToWin
{
    return 2;
}

- (id) flipRecordDesciptionText:(FlipRecord *)flipRecord;
{
    int score = flipRecord.score;
    NSArray *playingCards  = flipRecord.cardsInPlay;
    
    PlayingCard *card = nil;
    PlayingCard *otherCard = nil;
    if (playingCards.count) {
        card = [playingCards objectAtIndex:0];
        if (playingCards.count > 1) otherCard = [playingCards objectAtIndex:1];
    }
    
    if (score == 0) {
        return [NSString stringWithFormat:@"Flipped up %@", card.contents];
    } else if (score > 0) {
        return [NSString stringWithFormat:@"Matched %@ and %@ for %d points", card.contents, otherCard.contents, score];
    } else if (score < 0) {
        return [NSString stringWithFormat:@"%@ and %@ don't match! %d point penalty!", card.contents, otherCard.contents, score];
    }
    return nil;
}

- (void) updateCell:(UICollectionViewCell *)cell forCard:(Card *)card
{
    if ([cell isKindOfClass:[PlayingCardCollectionViewCell class]]) {
        if ([card isKindOfClass:[PlayingCard class]]) {
            PlayingCardCollectionViewCell *playingCardCell = (PlayingCardCollectionViewCell *) cell;
            PlayingCard *playingCard = (PlayingCard *) card;
            playingCardCell.playingCardView.rank = playingCard.rank;
            playingCardCell.playingCardView.suit = playingCard.suit;
            playingCardCell.playingCardView.faceUp = playingCard.faceUp;
            playingCardCell.playingCardView.alpha = playingCard.isUnplayable ? 0.3 : 1.0;
        }
    }
}

@end
