//
//  SetCardCollectionViewCell.h
//  Matchismo
//
//  Created by Bharath Krishnan on 2/23/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCardView.h"

@interface SetCardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet SetCardView *setCardView;

@end
